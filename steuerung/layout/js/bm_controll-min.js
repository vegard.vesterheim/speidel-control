var soundHandle;
var minFirmware = {major:1,minor:1,revision:24,year:2016,month:"Dec",day:2};
minFirmware.stamp = Date.parse(minFirmware.month+" "+minFirmware.day+", "+minFirmware.year);
var version = "App version: 0.9.14-0001 2017-09-12";
var shortterm = 3000;			//timerinterval
var secondssincestart = 0;		//Statustimer
var callTimeout = (shortterm)*4; //the interval for ajax timeouts. To make sure it's shorter than the timer interval it's half the timeinterval.

var currentResponseStringFromBM,currentResponseFromBM,bmstat,bmrz,bmtext,bmbuttons,brewOptions,rastPrototype,hoppingPrototype,allRecipes;
var stateInterval,intervalStopped,startMs,timer,setBMStatusFunctionVars,cObject;

var rezeptValue = 0;

var getLanguages = function(lng){
	
	var selectedLng;
	if(lng===""||lng===undefined){
		lng=0;
	}
	var lngs = ["de","en","fr","es"];
	if(lng<lngs.length){
		selectedLng = lngs[lng];
	}else{
		selectedLng = lngs[0];
	}
	$.ajax({
		url:protokol+hostname+basePath+'languages/getLanguage.php',
		timeout:callTimeout,
		data:'lng='+selectedLng,
		dataType: "json"
	}).done(function(data){
		//get New Language Contents and parse Dialogbox for reload page. 
		if(confirm(data.lngVars[103])){
			location.reload(true);
		}
		
		
	}).fail(function(obj,textStat,error){
		console.log("Mobile controll could not load new language.");
	});
	
};

/*
 *	communication() 
 *	This is the class of the communication object.	
*/
function communication(){
		this.getStart = function(){
			that = this;
			if(isNaN(that.callCounter)){
				that.callCounter = 0;
			}
			if(isNaN(that.callCounter)){
				that.callCounter = 0;
			}
			var versionRequest = $.ajax({
				url:bmvers,
				data:"",
				crossDomain:true,
				method:"GET",
				async:true,
				timeout:callTimeout,
				beforeSend:function(){
					console.log("Mobile Controll - loading firmware informations from device. (Try "+(that.callCounter)+"): "+(new Date()));
				}
			}).done(function(data){
				//successfully call for bm status.
				console.log("BM Firmware erhalten. "+(new Date()));
				that.callCounter = 0;				//reset the callCounter.
				firmwareVersionString	= data;
				var firmwareArrayTempArray = firmwareVersionString.split(" ");
				var firmwareArray = new Array();
				for(var i = 0;i<firmwareArrayTempArray.length;i++){
					if(firmwareArrayTempArray[i]!="" && firmwareArrayTempArray[i]!=undefined){
						firmwareArray.push(firmwareArrayTempArray[i]);
					}
				}
	
				firmwareDate = {month:firmwareArray[1],day:firmwareArray[2],year:firmwareArray[3]};
				var firmwareDateTime = Date.parse(firmwareDate.month+" "+firmwareDate.day+", "+firmwareDate.year );
				firmwareDate.stamp = firmwareDateTime;
				var firmwareVersionString = firmwareArray[0].slice(1);
				var firmwareVersionArray = firmwareVersionString.split(".");
				bmVersion 			= {major:parseInt(firmwareVersionArray[0]),minor:parseInt(firmwareVersionArray[1]),revision:parseInt(firmwareVersionArray[2])};
				//Firmware BM ausgeben. 
				$(".versioninfo").html($(".versioninfo").html()+"<br>BM-Firmware: "+bmVersion.major+"."+bmVersion.minor+"."+bmVersion.revision+" "+firmwareDate.day+". "+firmwareDate.month+". "+firmwareDate.year);
				if(bmVersion.major >= minFirmware.major && bmVersion.minor >= minFirmware.minor && bmVersion.revision >= minFirmware.revision && minFirmware.stamp <= firmwareDate.stamp){
					that.getStartData();
					console.log("Firmware match min. requirements. Get informations from BM.");	
				}else{
					currentResponseFromBM.buttons = new Array("0","0","0","rl");
					console.log(currentResponseFromBM.buttons);
					currentResponseFromBM.BrauStatus = 100;
					console.log("Firmware of your bm is too old.");
					$(".pageTitle").html(bmtext[104]);
					contents = pages.message.find(".display-content-row").clone();
					contents.eq(0).find("h2").html(bmtext[104]);
					contents.eq(1).find("p").html(bmtext[105]);
					showScreen(contents);
				}
			}).fail(function(obj,textStat,error){
				that.callCounter++;
				console.log("Mobile Controll - failed to load firmware informations from device. "+(new Date())+" (Try "+(that.callCounter)+")");
				console.log("Error Status: "+textStat);
				console.log("Error: "+error);
				console.log("Error Object: "+JSON.stringify({error:obj}));
				if(that.callCounter<10){
					console.log(callTimeout);
					waitTime = 2*callTimeout;
					console.log("Next try, waiting "+waitTime+"ms "+(new Date()));
					setTimeout( function(){
									that.getStart()
								}, 600);
				}else{
					console.log("Limit reached. stopping application. "+(new Date()));
					if(confirm(bmtext[106])){
						window.location.reload();
					}
				}
			});
			
			
		}

		/*
		 * getStartData();
		 * @return void();
		 * @params none;
		 * this function handles the initial status and recipe request on the connected Braumeister.
		 * the request for recipe-data starts if the request for bm status informations was successfull.	
		*/
		this.getStartData = function(){
			that = this;
			if(isNaN(that.callCounter)){
				that.callCounter = 0;
			}
			var request = $.ajax({
				url:bmstat,
				data:"k=0",
				crossDomain:true,
				method:'GET',
				async:true,
				timeout:callTimeout,
				beforeSend:function(){
					console.log("Mobiles Steuerung, Daten werden geladen (Versuch "+(that.callCounter)+"): "+(new Date()));
				}
			}).done(function(data){
				//successfully call for bm status.
				console.log("BM Status erhalten. "+(new Date()));
				that.callCounter = 0;				//reset the callCounter.
				currentResponseStringFromBM = data;	// write the collected status infomations to global status string.
				getCurrentResponseObject();			// create the status object
				//Versionscheck
				
				//Firmware BM ausgeben. 
				if(currentResponseFromBM.bmVersion.major >= minFirmware.major && currentResponseFromBM.bmVersion.minor >= minFirmware.minor && currentResponseFromBM.bmVersion.revision >= minFirmware.revision && minFirmware.stamp <= currentResponseFromBM.firmwareDate.stamp){
					that.getRecipes(1);
					console.log("Statusdaten in Objekt umgewandelt, Intervaltimer gestartet, Rezepte beim BM angefragt.");	
				}else{
					console.log(currentResponseFromBM.buttons);
					currentResponseFromBM.buttons = new Array("0","0","0","rl");
					console.log(currentResponseFromBM.buttons);
					currentResponseFromBM.BrauStatus = 100;
					console.log("Firmware of your bm is too old.");
					$(".pageTitle").html(bmtext[104]);
					contents = pages.message.find(".display-content-row").clone();
					contents.eq(0).find("h2").html(bmtext[104]);
					contents.eq(1).find("p").html(bmtext[105]);
					showScreen(contents);
				}
				
			}).fail(function(obj,textStat,error){
				that.callCounter++;
				console.log("Mobile Steuerung - Status konnte nicht geladen werden. "+(new Date())+" (Versuch "+(that.callCounter)+")");
				console.log("Fehler Status: "+textStat);
				console.log("Fehler: "+error);
				console.log("Fehler Objekt: "+obj);
				if(that.callCounter<10){
					console.log(callTimeout);
					waitTime = 2*callTimeout;
					console.log("Initiiere neuen Versuch, Warte "+waitTime+"ms "+(new Date()));
					setTimeout( function(){
									that.getStartData()
								}, 600);
				}else{
					console.log("Versuchlimit überschritten. Stoppe Applikation. "+(new Date()));
					if(confirm(bmtext[106])){
						window.location.reload();
					}
				}
			});
		}
		/*callBM()*/
		this.callBM = function(dataString,callUrl,callMethod){
		if(callMethod==="" || callMethod===undefined || callMethod==="undefined"){
			callMethod = "GET";
		}
		var that = this;
		var request = $.ajax({
			url:callUrl,
			data:dataString,
			crossDomain:true,
			method:callMethod,
			async:true,
			timeout:callTimeout,
			beforeSend:function(){
				var d = new Date();
				var n = d.toTimeString();					
			}
		}).done(function(data){
			var d = new Date();
			var n = d.toTimeString();
			currentResponseStringFromBM = data;
			this.callCounter = 0;
			return true;
		}).fail(function(obj,textStat,error){
			console.log("Error date: "+ (new Date()));
			console.log("Error: "+textStat);
			console.log(error);
			console.log(obj);
			if(isNaN(that.callCounter)){
				that.callCounter = 0;
			}
			
			if(that.callCounter< 10){
				that.callCounter++;
				setTimeout(function(){
					that.callBM(dataString,callUrl,callMethod);
				}, 600);
			}else{
				console.log("Recall "+callUrl+" overflow. Stop the requests.");
				if(confirm("Try "+that.callCounter+" times to request at "+callUrl+". Request failed, would you try again?")){
					that.callCounter = 0;
					setTimeout(function(){
						that.callBM(dataString,callUrl,callMethod);
					}, 600);
				}else{
					window.location.reload();
				}
				that.callCounter = 0;
			}
			return false;
		});
		return request;
	};
	this.getStatus = function(){
		var othis = this;
		$.ajax({
			url:bmstat,
			data:"k=0",
			crossDomain:true,
			method:'GET',
			async:true,
			timeout:callTimeout,
			beforeSend:function(){
				
			}
		}).done(function(data){
                    // console.log ("Got data from speidel");
			currentResponseStringFromBM = data;
			getCurrentResponseObject();
			return data;
			
		}).fail(function(obj,textStat,error){
			console.log("Fehler Datum: "+ (new Date()));
			console.log(obj);
			console.log(textStat);
			console.log(error);
			
			//try again
			if(isNaN(othis.callCounter))othis.callCounter = 0;
			othis.callCounter++;
			if(othis.callCounter<10){
				setTimeout(function(){
					othis.getStatus();
				}, 600);
			}else{
				console.log("Recall "+bmstat+" overflow. Stop the requests.");
				if(confirm("try "+othis.callCounter+" times to connect to "+bmstat+". Request failed, would you try again?")){
					othis.callCounter = 0;
					setTimeout(function(){
						othis.getStatus();
					}, 600);
				}else{
					window.location.reload();
				}
				othis.callCounter = 0;
			}
		});
	};
	this.getRecipes = function(isstart){
		var othis = this;
		if(isNaN(othis.callCounter)){
			othis.callCounter = 0;
		}
		if(isstart==1){
			othis.isstart = isstart;
			console.log("Rezeptaufruf bei Programmstart ");	
		}else{
			othis.isstart = 0;
		}
		
		console.log("Versuch "+(othis.callCounter)+" "+(new Date()));
		$.ajax({
			url:bmrz,
			data:"k=0",
			//cache:false,
			crossDomain:true,
			method:'GET',
			async:true,
			timeout:callTimeout,
			beforeSend:function(obj){
				console.log("Before Send ("+bmrz+"): "+ new Date());
			}
		}).done(function(data){
			if(isstart==1){
				console.log("Programmstart.");
			}
			console.log("Rezeptdaten empfangen "+(new Date()));
			if(allRecipes.rawData !== data){
				//set new raw-Data
				//delete old recipes and create new.
				allRecipes = new RecipesObject();
				allRecipes.rawData = data;
				var recipesData = data.split("\n");
				//var numOfRecipes = recipesData[0];
				for(var i=1;i<recipesData.length;i++){
					allRecipes.addRecipe(recipesData[i]);
				}
				allRecipes.loaded = true;
			}
			if(isstart==1){
				//@Todo hier gibt es noch potential für Verbesserungen (evtl. das Statusinterval anderst starten).
				secondssincestart = 0;				// make sure the timer time is 0.
				timer = setInterval(timer, 1000); 	// start the timers.
				buildScreen();
				intervalStopped = false;			// initial set of stop flag.
				stateInterval = setTimeout(updateStates, shortterm);	//start the "status game"
			}
		}).fail(function(obj,textStat,error){
			if(othis.isstart==1){
				console.log("Programmstart fehlgeschlagen.");
			}
			console.log("Fehler Datum: "+ (new Date()));	
			console.log("Fehler: "+textStat);
			console.log("Fehler Objekt: "+error);
			console.log("Anfrageobjekt: "+obj);		
			//try again
			if(isNaN(othis.callCounter)){
				othis.callCounter = 0;
			}
			othis.callCounter++;
			if(othis.callCounter<10){
				console.log("Initiiere neuen Versuch, Warte "+(2*callTimeout)+"ms "+(new Date()));
				if(textStat=="error"){
					setTimeout(function(){othis.getRecipes(othis.isstart)}, 600);

				}else{
					setTimeout(function(){othis.getRecipes(othis.isstart)}, 600);
				}
			}else{
				console.log("Recall limit reached. Stop the requests.");
				if(confirm("try "+othis.callCounter+" times to connect to "+bmrz+". Request failed, would you try again?")){
					othis.callCounter = 0;
					if(textStat=="error"){
					
						setTimeout(function(){othis.getRecipes(othis.isstart)}, 600);
					}else{
						setTimeout(function(){othis.getRecipes(othis.isstart)}, 600);
					}
				}else{
					window.location.reload();
				}
			}
			
			//othis.getRecipes();
		});
	};
}

/*
 * Object recipeObject() 
 * @params {dataString}
*/
function recipeObject(rString){
	/*
	 * setData() 
	 * function to set recipe Data from given String.
	*/
	this.setData = function(dataString){
		var dataAndNameArray = dataString.split(".");
		
		var dataArray = dataAndNameArray[0].split("X");
		var nameString = dataAndNameArray[(dataAndNameArray.length-1)];
		if(nameString.trim()===""){
			nameString = bmtext[13]+' '+(parseInt(dataArray[0])+1);
		}
		this.recipeId = (parseInt(dataArray[0]));
		this.name = nameString;
		this.einmaischTemp = parseFloat(dataArray[1]);
		this.rast = [
			{ id:1,
			  temp:dataArray[2],
			  time:parseInt(dataArray[3])
			},
			{id:2,
			  temp:dataArray[4],
				time:parseInt(dataArray[5])
			},
			{id:3,
			  temp:dataArray[6],
				time:parseInt(dataArray[7])
			},
			{id:4,
			  temp:dataArray[8],
				time:parseInt(dataArray[9])
			},
			{id:5,
			  temp:dataArray[10],
				time:parseInt(dataArray[11])
			}
		];
		var igaben = [];
		for(var i=0;i<6;i++){
			igaben.push(dataArray[(14+i)]);
			if(dataArray[(14+i)]!=="" && parseInt(dataArray[(14+i)])>0){
				//igaben.push(dataArray[(14+i)]);
			}
		}
		this.hopfengaben = {
			duration:dataArray[12],
			temp:parseFloat(dataArray[13]),
			gaben:igaben
		};
	};
	/* writeRecipe();
	* This function fill the recipe data in $('.recipe-process')
	* return bool	
	*/
	this.writeRecipe = function(){
		var insertRast,rastnumber,inserthopping;
		$(".brew-process #hops .hoppings-ul").empty();
		$(".brew-options .brew .recipeName").html(bmtext[13]+": "+this.name);
		$("#einmaischen .temperature-set").html(this.einmaischTemp+currentResponseFromBM.unit);
		$(".brew-process .brew-process-li.rast").remove();
		rastnumber = 1;
		for(var i=0;i<this.rast.length;i++){
			//if(this.rast[i].time>0){
				insertRast = rastPrototype.clone();
				insertRast.prop("id","rast_"+this.rast[i].id);
				if(this.rast[i].time==0){
					insertRast.addClass("disabled");
				}
				insertRast.find("h3>.number").html(rastnumber+".");
				insertRast.find(".info-box-header h3>span").html(bmtext[96]+rastnumber+". "+bmtext[24]);
				insertRast.find(".info-box-content>figure>img").prop("alt",bmtext[24])
				if(i==(this.rast.length - 1)){
					insertRast.find(".info-box-content>figure>img").prop("src","layout/images/5-rast.gif").prop("alt",bmtext[18]);
					
				}
				insertRast.find(".info-box-content-text").html($("<p>").append(bmtext[(84+i)]));
				insertRast.find(".temperature-set").html(this.rast[i].temp+currentResponseFromBM.unit);
				insertRast.find(".temperature-actual").html(bmtext[24]+": "+currentResponseFromBM.IstTemp+currentResponseFromBM.unit);
				insertRast.find(".time-actual").html(currentResponseFromBM.IstTimeString);
				insertRast.find(".time-set").html(this.rast[i].time+bmtext[92]);
				insertRast.insertBefore(".brew-process #hops");
				rastnumber++;
			//}
		}
		$(".brew-process #hops .recipe-detail").find(".temperature-set").html(this.hopfengaben.temp+currentResponseFromBM.unit);
		$(".brew-process #hops .recipe-detail").find(".time-set").html(this.hopfengaben.duration+bmtext[92]);
		for(var j=0;j<this.hopfengaben.gaben.length;j++){
			inserthopping = hoppingPrototype.clone();
			inserthopping.find(".title").html((j+1)+bmtext[34]+this.hopfengaben.gaben.length+bmtext[35]);
			inserthopping.find(".time-set").html(this.hopfengaben.gaben[j]+" "+bmtext[36]);
			if(this.hopfengaben.gaben[j]==0){
				inserthopping.addClass("disabled");
			}
			$(".brew-process #hops .hoppings-ul").append(inserthopping);
		}
		return true;
	};
	this.setData(rString);
}
/*
 * Object RecipesObject()
 * store all recipes from the connected Braumeister.
 *
*/
function RecipesObject(){
	
	this.recipes = [];
	/*
	* addRecipe();
	* @params {rString: String of the recipe to add to the stored recipes} 	
	*/
	this.addRecipe = function(rString){
		var recipe = new recipeObject(rString);
		this.recipes.push(recipe);
	};
	/* getRecipeByIndex()
	 * @params{rIndex:Index of the selected recipe}
	 *	
	*/
	this.getRecipeByIndex = function(rIndex){
		return this.recipes[rIndex];
	};
	/* getRecipeByName();
	 *	@params {rName: Name of the recipe to search for}
	*/
	this.getRecipeByName = function(rName){
		for(var i=0;i<this.recipes.length;i++){
			if(this.recipes[i].name === rName){
				return this.recipes[i];
			}
		}
	};
	/*
	 * reselectRecipe(upDown);
	 * @params {upDown: Value of the pushed Button, 2 for down, 3 for up}
	 * With this function is needed to reselect a recipe from the recipelist in automatic mode.
	 * after move the cursor on the mobile controll the function makes an setBMStatus call with the button pushed by user.
	*/
	this.reselectRecipe = function(upDown){
		var currentRecipe = currentResponseFromBM.aktRezept;
		var recipeToLoad = currentRecipe;
		if(upDown===2){
			
			if(currentResponseFromBM.aktRezept < (this.recipes.length-1)){ //do not change recipe upwards if the last recipe in list is selected
				recipeToLoad++;
			}else{
				recipeToLoad = 0;
			}
		}else if(upDown===3){
			if(currentResponseFromBM.aktRezept>0){ //do not change recipe downwards if the first recipe in list is selected
				recipeToLoad--;
			}else{
				recipeToLoad = this.recipes.length-1;
			}
		}
		if(recipeToLoad !== currentRecipe){ 	//check if is neccessary to load the new recipe.
			$("#rezepte .select-recipe").removeClass("selectedRecipe");
			$("#recipe-"+(recipeToLoad+1)).addClass("selectedRecipe");
			brewOptions.find(".brew").fadeOut("fast");
			if(this.recipes[recipeToLoad] && this.recipes[recipeToLoad].writeRecipe()){
				brewOptions.find(".brew").fadeIn("slow");
				setBmStatus(upDown,false,true);	//write recipe to brew
			} 
					
		}
	};
}
/*
 *	renameButtons()
 * 	return void();
 *	Function to rename Buttons after BM Status has changed. use the currentResponseFromBM.buttons Array to rebuild Buttons (Name and image)	
 */
var renameButtons = function(){
	var currentButtonSet = currentResponseFromBM.buttons;
	var buttons = $(".display-content-navigation-ul li");
	var roundbuttons = $(".display-navigation-ul li");
	
	buttons.css({
		visibility:"hidden"
	});
	buttons.addClass("disabled");
        roundbuttons.addClass("disabled");
        try {
 	for(var i = 0;i< currentButtonSet.length;i++){
		if(currentButtonSet[i]!=="0" && currentButtonSet[i]!==""){
			buttons.eq(i).prop("id",bmbuttons[currentButtonSet[i].toLowerCase()].title);
			buttons.eq(i).prop("title",bmbuttons[currentButtonSet[i].toLowerCase()].text).find("span").html(bmbuttons[currentButtonSet[i].toLowerCase()].text);
			if(currentResponseFromBM.BrauStatus<6000){
				buttons.eq(i).css({
					visibility:"visible"
				}).removeClass("disabled");
				roundbuttons.eq(i).removeClass("disabled");
			}else{
				if(currentButtonSet[i].toLowerCase()==="a"){
					buttons.eq(i).css({
						visibility:"visible"
					}).removeClass("disabled");
					roundbuttons.eq(i).removeClass("disabled");
				}				
			}
		}
	}
        } catch(err) {
    }
};
/*
 * getCurrentResponseObject();
 * build the statusObject from the BM Statusstring. 	
 */
var getCurrentResponseObject = function(){
	oldResponseObject = currentResponseFromBM;
	currentResponseFromBM = new Object();
	currentResponseFromBM.rawData = currentResponseStringFromBM;
	var currentResponseArray = currentResponseFromBM.rawData.split(";");
	currentResponseFromBM.firmwareVersionString	= currentResponseArray[0];
	var firmwareArrayTempArray = currentResponseArray[0].split(" ");
	var firmwareArray = new Array();
	for(var i = 0;i<firmwareArrayTempArray.length;i++){
		if(firmwareArrayTempArray[i]!="" && firmwareArrayTempArray[i]!=undefined){
			firmwareArray.push(firmwareArrayTempArray[i]);
		}
	}
	
	currentResponseFromBM.firmwareDate = {month:firmwareArray[1],day:firmwareArray[2],year:firmwareArray[3]};
	var firmwareDateTime = Date.parse(currentResponseFromBM.firmwareDate.month+" "+currentResponseFromBM.firmwareDate.day+", "+currentResponseFromBM.firmwareDate.year );
	currentResponseFromBM.firmwareDate.stamp = firmwareDateTime;
    try {
	var firmwareVersionString = firmwareArray[0].slice(1);
    } catch (err) {
        console.log("Invalid firmwareArray. No response from speidel?");
        sollTempString = oldResponseObject.sollTemp;
        return false;

    }
	var firmwareVersionArray = firmwareVersionString.split(".");
	currentResponseFromBM.bmVersion 			= {major:parseInt(firmwareVersionArray[0]),minor:parseInt(firmwareVersionArray[1]),revision:parseInt(firmwareVersionArray[2])};
	currentResponseFromBM.bmId = currentResponseArray[1];

    try {
	var dataArray = currentResponseArray[2].split("X");
    } catch(err) {
        console.log("Invalid currentResponseArray. No response from speidel?");
        sollTempString = oldResponseObject.sollTemp;
        return false; 

    }
	currentResponseFromBM.rawArray   	= dataArray;
	currentResponseFromBM.language   	= parseInt(dataArray[0]);
	currentResponseFromBM.uhrzeit  	 	= dataArray[1];
	var unit = dataArray[2];
	if(unit === "" || unit === undefined || unit.toLowerCase()==="c"){
		unit = bmtext[93];
	}else{
		unit = bmtext[94];
	}
	currentResponseFromBM.unit			= unit;

	currentResponseFromBM.BrauStatus 	= parseInt(dataArray[3]);
	var sollTempString = dataArray[4];
	try{
		sollTempString = sollTempString.trim();
	}catch(err){
		console.log("Konnte String der Soll-Temperatur nicht kürzen.");
		console.log("Fehler: "+err);
		console.log("Datenarray: "+dataArray);
		console.log("Setze alten Wert: ");
		sollTempString = oldResponseObject.sollTemp;
	}
	
	var sollTemp = parseFloat(sollTempString);
	if(sollTemp<0){
		sollTemp = oldResponseObject.SollTemp;
	}
	currentResponseFromBM.SollTemp 		= sollTemp/10;
	currentResponseFromBM.IstTemp 		= parseFloat(dataArray[5]);
	currentResponseFromBM.SollTime 		= parseInt(dataArray[6]);

		var sollTimeString = parseInt(dataArray[6]);
		var sollTimeSeconds = sollTimeString % 60;
		var sollTimeMinutes  = Math.floor((sollTimeString/60));
		var sollTimeHours = 0;
		if(sollTimeMinutes>=60){
			//sollTimeHours = Math.floor((sollTimeMinutes/60));
			//sollTimeMinutes = sollTimeMinutes % 60;
		}
	currentResponseFromBM.SollTimeMinutes = sollTimeMinutes;
	currentResponseFromBM.SollTimeSeconds = sollTimeSeconds;
		
	currentResponseFromBM.SollTimeString = ((sollTimeHours>0)?sollTimeHours+':':'')+sollTimeMinutes+((sollTimeMinutes<10)?((sollTimeSeconds<10)?":0"+sollTimeSeconds:":"+sollTimeSeconds):'');
		var istTimeString = parseInt(dataArray[7]);
		var istTimeSeconds = istTimeString % 60;
		var istTimeMinutes  = Math.floor((istTimeString/60));
		var istTimeHours = 0;
		/* @ deprecated if(istTimeMinutes>=60){
			istTimeHours = Math.floor((istTimeMinutes/60));
			istTimeMinutes = istTimeMinutes % 60;
		}*/
		
	currentResponseFromBM.IstTime 		= parseInt(dataArray[7]);
	currentResponseFromBM.IstTimeString = istTimeMinutes+":"+((istTimeSeconds<10)?"0"+istTimeSeconds:istTimeSeconds);
	//second type of istTimeString now with hours.
		if(istTimeMinutes>=60){
			istTimeHours = Math.floor((istTimeMinutes/60));
			istTimeMinutes = istTimeMinutes % 60;
		}
	
	currentResponseFromBM.IstTimeStringWH = ((istTimeHours>0)?istTimeHours+':':'')+istTimeMinutes+":"+((istTimeSeconds<10)?"0"+istTimeSeconds:istTimeSeconds);
	
	 
	currentResponseFromBM.aktRezept		= parseInt(dataArray[8]); 
	if(isNaN(parseInt(dataArray[9]))){
		dataArray[9] = 0;
	}
	currentResponseFromBM.currentRast	= parseInt(dataArray[9]);
	currentResponseFromBM.hopfenGabe	= parseInt(dataArray[10]);
	currentResponseFromBM.progress 		= (parseInt(dataArray[11])/318*100);
	
	var buttons = dataArray[12].split("");
	if(parseInt(dataArray[3])<2000){
		for(var b=0;b<buttons.length;b++){
			if(buttons[b].toLowerCase()==="s"){
				buttons[b] = "bs";
			}
		}	
	}
	currentResponseFromBM.buttons 		= buttons;
	var phxArray = dataArray[13].split("");
	currentResponseFromBM.pumpe	 		= phxArray[0];
	currentResponseFromBM.heizung 		= (phxArray[1]!=="")?phxArray[1]:'O';	
	var beep = dataArray[14].split("");
	currentResponseFromBM.interrupt		= parseInt(beep[0]);
	if(beep[1]=="D"){
		currentResponseFromBM.interrupt	= 1;
		currentResponseFromBM.beep		= 1;
		currentResponseFromBM.cover		= 1;
	}else{
		currentResponseFromBM.cover		= 0;
		currentResponseFromBM.beep = parseInt(beep[1]);
	}
	currentResponseFromBM.editMode		= beep[2];
};
var pages = {};

/*
 * @deprecated 	
 * requestRecipes();
 * request all recipes from the connected bm.
 * @return object, jQuery ajax-object.	
*/
/*
var requestRecipes = function(statusId,async){
	 return $.ajax({
		url:bmrz,
		data:"k="+statusId,
		//cache:false,
		crossDomain:true,
		method:'GET',
		timeout:callTimeout,
		async:async,
	});
};
*/

/*
 *	updateScreen()
 *	Update the Screen and other Elements on the mobile-Page. 
 *  Three updates. First Fix Elements of the Screen like pump and heating 
 *	second Update the screen specific content. 
 *	third Rename the buttons.
*/
var updateScreen = function(){
	console.log("updateScreen called");
	//Status pump
	var Bild1,Bild2;
	//Update Time 
	$(".display-status-time time,.header-display-status-time").html(currentResponseFromBM.uhrzeit);
        if (currentResponseFromBM.IstTemp === undefined) {
           return;
        }
        //Update Temps:
	$("body").find(".display-status-temperatur,.temperature-actual").html(currentResponseFromBM.IstTemp.toFixed(1)+currentResponseFromBM.unit);
	
	//Update Times
	$("body").find(".time-actual").html(currentResponseFromBM.IstTimeString);
	
	if(((currentResponseFromBM.BrauStatus>4006 && currentResponseFromBM.BrauStatus<= 4007)||(currentResponseFromBM.BrauStatus>=3010 && currentResponseFromBM.BrauStatus<= 3080)) && currentResponseFromBM.editMode=="T"){
		if(!($("body").find(".fixTemp").find("input").is(":focus"))){
			$("body").find(".fixTemp").find("input").val(currentResponseFromBM.SollTemp.toFixed(0));
		}
	}else if((currentResponseFromBM.BrauStatus>=3010 && currentResponseFromBM.BrauStatus<= 3080) && currentResponseFromBM.editMode=="Z"){
		if(!($("body").find(".display-content .time-set").find("input").is(":focus"))){
			$("body").find(".display-content .time-set").find("input").val(currentResponseFromBM.SollTimeMinutes.toFixed(0));
		}
	}else{
		$("body").find(".fixTemp").html(currentResponseFromBM.SollTemp.toFixed(0));
		$("body").find(".display-content .time-set").html(currentResponseFromBM.SollTimeString);
	}
	
	//Update Runtime and set Time fields
	
	//$("body").find(".time-set").html(currentResponseFromBM.SollTimeString);
	$("body").find(".runtime").html(currentResponseFromBM.IstTimeString+" "+bmtext[92]);
	
	if(currentResponseFromBM.interrupt===1){
		if(!($(".display-panel").hasClass("alert"))){
			$(".display-panel").addClass("alert");
		}
	}else if(currentResponseFromBM.interrupt===0){
		if($(".display-panel").hasClass("alert")){
			$(".display-panel").removeClass("alert");
		}
	}
	$('.header-display-status-link').removeClass("not-connected").addClass("connected");
	if ( currentResponseFromBM.pumpe === 'P') {
		Bild1 = 'ledp0.bmp';			
		if(! $('.display-status-pump').hasClass("current")) { //pump on
			$('.display-status-pump').addClass('current');
		}
		if($('.display-status-pump').hasClass("inactive")) {
			$('.display-status-pump').removeClass('inactive');
		}
	}else if(currentResponseFromBM.pumpe === 'q') { // pump on but not working (error)
		if(! $('.display-status-pump').hasClass("inactive")) {
			$('.display-status-pump').addClass('inactive');
		}
		if($('.display-status-pump').hasClass("current")) {
			$('.display-status-pump').removeClass('current');
		}
	}else {	//pump off
		Bild1 = 'ledp1.bmp';
		if( $('.display-status-pump').hasClass("current") || $('.display-status-pump').hasClass("inactive")) {
			$('.display-status-pump').removeClass('current inactive');
		} 

	}
	//status sound
	console.log("beep");
	console.log(currentResponseFromBM.beep);
	if(currentResponseFromBM.beep==2 || currentResponseFromBM.beep==1){
		console.log("play");
		soundHandle.play();
	}else{
		console.log("sound of silence");
		soundHandle.pause();
	}
	//status sound ende
	//Status heating
	if ( currentResponseFromBM.heizung === 'H') {		
		Bild2 = 'ledh0.bmp';
		if(! $('.display-status-heater').hasClass("current")) { // heating on
			$('.display-status-heater').addClass('current');
		}
		if($('.display-status-heater').hasClass("inactive")) {
			$('.display-status-heater').removeClass('inactive');
		}
	}else if(currentResponseFromBM.heizung === 'i') { // heating on but not working (error)
		if(! $('.display-status-heater').hasClass("inactive")) {
			$('.display-status-heater').addClass('inactive');
		}
		if($('.display-status-heater').hasClass("current")) {
			$('.display-status-heater').removeClass('current');
		}
	}else { // heating off
		Bild2 = 'ledh1.bmp';
		if( $('.display-status-heater').hasClass("current") || $('.display-status-heater').hasClass("inactive")) {
			$('.display-status-heater').removeClass('current inactive');
		}
	}
	
	//$(".varTemp").html(currentResponseFromBM.IstTemp+currentResponseFromBM.unit);
	/*change contents on the right side and temp and time in the main screen.*/
	
	$(".brew-process-li").removeClass("current");
	switch (true){
		default:
			$( ".display-status-bar-percent" ).css({ 'width': '0%' });
		break;
		case(currentResponseFromBM.BrauStatus>=2030 && currentResponseFromBM.BrauStatus<2810):
		/* mashing*/
			$(".brew-process-li").eq(0).addClass("current");
			$( ".display-status-bar-percent" ).css({ 'width': currentResponseFromBM.progress+'%' });
		break;
		/*rest */
		case(currentResponseFromBM.BrauStatus>=2810 && currentResponseFromBM.BrauStatus < 2910):
			$(".brew-process-li").eq(0).removeClass("current").addClass("done");
			$(".brew-process-li.rast").each(function(itemindex){
				if(itemindex<currentResponseFromBM.currentRast && !($(this).hasClass("done") )){
					$(this).addClass("done");
				}
			});
			
			
			$( ".display-status-bar-percent" ).css({ 'width': currentResponseFromBM.progress+'%' });
			$(".brew-process-li.rast").eq(currentResponseFromBM.currentRast).addClass("current");
			
			selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
			$(".pageTitle").html(selectedRecipe.name+" - "+(currentResponseFromBM.currentRast+1)+". "+ bmtext[24]);
			$(".display-screen").find(".fixTemp").html(selectedRecipe.rast[currentResponseFromBM.currentRast].temp);
			$(".display-screen").find(".time-set").html(selectedRecipe.rast[currentResponseFromBM.currentRast].time);


		break;
		/*hoping*/
		case(currentResponseFromBM.BrauStatus>=2920 && currentResponseFromBM.BrauStatus < 3450):
			$(".brew-process-li").eq(0).removeClass("current").addClass("done");
			$(".brew-process-li.rast").each(function(){
				if(!($(this).hasClass("done") )){
					$(this).addClass("done");
				}
			});
			$(".brew-process-li#hops").addClass("current");
			$(".hoppings-li").each(function(itemindex){
				if(itemindex<currentResponseFromBM.hopfenGabe && !($(this).hasClass("done") )){
					$(this).addClass("done");
				}else if(itemindex == currentResponseFromBM.hopfenGabe&& !($(this).hasClass("current") )){
					$(this).addClass("current");
				}
			});
			$( ".display-status-bar-percent" ).css({ 'width': currentResponseFromBM.progress+'%' });
			
		break;
	}
};
/*
 * createRecipesList();
 * this function collect all recipes from the allRecipes object and returns the resulted html to the buildScreen function
 * @return object, jQuery-object of the generated content.	
*/
var createRecipesList = function(){
	var lList = $("<div>").addClass("col-left-half");
	var rList = $("<div>").addClass("col-right-half").addClass("txt-left");
	for(var i = 0; i< allRecipes.recipes.length;i++){
		var currentRecipe = allRecipes.getRecipeByIndex(i);
		var curRecObject = $("<p>").append($("<span>").html(currentRecipe.name)).prop("id","recipe-"+(i+1)).addClass("select-recipe");
		if(currentResponseFromBM.aktRezept === currentRecipe.recipeId){
			curRecObject.addClass("selectedRecipe");
		}
		if(i<5){
			lList.append(curRecObject);	
		}else{
			rList.append(curRecObject);	
		}
	}
	return $("<div>").prop("id","rezepte").append(lList).append(rList);
};
/*
 * buildAndShowRecipe();
 * this function writes the selected recipe on the right side of the screen.	
 * @return nothing.
*/
var buildAndShowRecipe = function(){
	var selectedRecipe;
	if(currentResponseFromBM.BrauStatus >2000 &&currentResponseFromBM.BrauStatus <3450){
		selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
		if(selectedRecipe && selectedRecipe.writeRecipe()){
			brewOptions.find(".brew").fadeIn("slow");
			
			$("#rezepte .select-recipe").removeClass("selectedRecipe");
			$("#recipe-"+(currentResponseFromBM.aktRezept+1)).addClass("selectedRecipe");
			
			
			
			
		}
	}
};
/**
 * build the Current Screen .
 * 
 * @return nothing.
 */
var buildScreen = function(){
	if(currentResponseFromBM.BrauStatus<=2015){
		brewOptions.children("div").fadeOut("fast");
	}	
	var contents,content,contentString,selectedRecipe,temperaturString,recipeTime,recipeTimeString;
	/*first check Interrupt and show Interrupt Message.*/
	if(currentResponseFromBM.interrupt>0){
		content = pages.message.clone();
		if(currentResponseFromBM.interrupt>1){
			content.find("h2").html(bmtext[95]);
			content.find("p").empty();
			showScreen(content.find(".display-content-row"));
		}else{
			if(currentResponseFromBM.cover==1){
				content.find("h2").html(bmtext[107]);
			}else{
				content.find("h2").html(bmtext[37]);
			}
			showScreen(content.find(".display-content-row"));
		}
	}else{
		/*No Interrupt state. Show main content.*/
		switch (true){
			/* Startpage of BM-Controll*/
			default:
			case(currentResponseFromBM.BrauStatus<2000):
				$(".pageTitle").html(bmtext[55]);
				contents = pages.startseite.find(".display-content-row").clone();
				contents.find("p").html("");
				
				showScreen(contents);
				
				brewOptions.find(".startinfo").fadeIn("slow");
			break;
			/*Automatic recipelist.*/
			case(currentResponseFromBM.BrauStatus>=2000 && currentResponseFromBM.BrauStatus<=2020):
				$(".pageTitle").html(bmtext[80]);
				content = pages.automatik.find(".display-content-row").eq(0).clone();
				content.empty();		
				var rezepte = createRecipesList();
				buildAndShowRecipe();
				content.append(rezepte);
				showScreen(content);
				
			break;
			 /*Automatic Step One.*/
			case(currentResponseFromBM.BrauStatus > 2020 && currentResponseFromBM.BrauStatus<2200):
				content = pages.automatik_step.clone();
				temperaturString = "";
				selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
				$(".pageTitle").html(selectedRecipe.name+" - "+bmtext[7]);
				
				content.find(".fixTemp").html(selectedRecipe.einmaischTemp);
				content.find(".varTemp").html(currentResponseFromBM.IstTemp.toFixed(1)+currentResponseFromBM.unit);
				buildAndShowRecipe();
				showScreen(content.find(".display-content-row"));				
			break;
			/*Automatik Step two, exhaust the air from pump */
		        case(currentResponseFromBM.BrauStatus>=2200 && currentResponseFromBM.BrauStatus<2400):
                                // FIXME, this code fails if recipes have been deleted since loading
                                // selectedRecipe becomes undefined
				selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
				$(".pageTitle").html(selectedRecipe.name+" - "+bmtext[7]);
				contentString = bmtext[11];
				contents = pages.automatik.find(".display-content-row").clone();
				content = contents.eq(0);
				content.find("p").remove();
				content.find("h2").html(contentString);
				buildAndShowRecipe();
				showScreen(content);
			break;
			/*Automatic step three, heat for mash*/
                        case(currentResponseFromBM.BrauStatus>=2400 && currentResponseFromBM.BrauStatus<2500):
                                // FIXME, this code fails if recipes have been deleted since loading
                                // selectedRecipe becomes undefined
				selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
				contents = pages.automatik_step_tt.find(".display-content-row").clone();
				
				$(".pageTitle").html(selectedRecipe.name+" - "+bmtext[9]);		
				contents.find(".fixTemp").html(selectedRecipe.einmaischTemp+currentResponseFromBM.unit);
				contents.find(".varTemp").html(currentResponseFromBM.IstTemp.toFixed(1)+currentResponseFromBM.unit);
				buildAndShowRecipe();
		
				showScreen(contents.eq(0));
			break;
			/*mash temp. reached*/
                        case(currentResponseFromBM.BrauStatus>=2510 && currentResponseFromBM.BrauStatus<2610):
                                // FIXME, this code fails if recipes have been deleted since loading
                                // selectedRecipe becomes undefined
				selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
				if(currentResponseFromBM.pumpe == "q"){
					$(".pageTitle").html(selectedRecipe.name+" - "+ bmtext[26]);
					contents = pages.message.find(".display-content-row").clone();
					contents.eq(0).find("h2").html(bmtext[26]);
					var text = bmtext[27]+"<br>"+bmtext[28]+"<br>";
					contents.eq(1).find("p").html(text);
				}else{
					contents = pages.automatik_step.find(".display-content-row").clone();
				
					$(".pageTitle").html(selectedRecipe.name+" - "+bmtext[8]);
					contents.find(".fixTemp").html(currentResponseFromBM.SollTemp.toFixed(0));
					contents.find(".temperature-actual").html(currentResponseFromBM.IstTemp.toFixed(1)+currentResponseFromBM.unit);
					contents.eq(1).find("h2").html(selectedRecipe.einmaischTemp.toFixed(1)+currentResponseFromBM.unit+bmtext[21]);
				}
			
				
				
				buildAndShowRecipe();
				
				showScreen(contents);
			break;
			/*insert mash order*/ /*mash insert question*/
			case(currentResponseFromBM.BrauStatus>=2610 && currentResponseFromBM.BrauStatus<2810):
				selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
				contents = pages.automatik_step.find(".display-content-row").clone();
				var title = "";
				if(currentResponseFromBM.BrauStatus<2705){
					title = title+bmtext[15];
				}else{
					title = title+bmtext[16];
				}
				contents.find(".fixTemp").html(currentResponseFromBM.SollTemp);
				contents.find(".temperature-actual").html(currentResponseFromBM.IstTemp.toFixed(1)+currentResponseFromBM.unit);
				$(".pageTitle").html(selectedRecipe.name+" - "+title);
				contents.eq(1).find("h2").html(title);
				buildAndShowRecipe();
				showScreen(contents);
				
			break;
			/*Automatik rast heating*/
			case(currentResponseFromBM.BrauStatus>=2810 && currentResponseFromBM.BrauStatus<2910):
				console.log("Rast aufgerufen: "+currentResponseFromBM.currentRast);
				
				selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
				var actualRast = currentResponseFromBM.currentRast;
				if(actualRast>=selectedRecipe.rast.length){
					actualRast = 0;
				}
				/*break;
					Automatik rast time runs
				*/				
				
				if(currentResponseFromBM.pumpe == "q"){
					$(".pageTitle").html(selectedRecipe.name+" - "+ bmtext[25]);
					contents = pages.message.find(".display-content-row").clone();
					contents.eq(0).find("h2").html(bmtext[25]);
					contents.eq(1).find("p").html(bmtext[30]);
				}else{
					contents = pages.automatik_step_tt.find(".display-content-row").clone();
					$(".pageTitle").html(selectedRecipe.name+" - "+(currentResponseFromBM.currentRast+1)+". "+ bmtext[24]);
					temperaturString = selectedRecipe.rast[actualRast].temp+" / "+currentResponseFromBM.IstTemp.toFixed(1)+currentResponseFromBM.unit;
					contents.find(".fixTemp").html(selectedRecipe.rast[actualRast].temp);
					contents.find(".temperature-actual").html(currentResponseFromBM.IstTemp.toFixed(1)+currentResponseFromBM.unit);
					recipeTime = selectedRecipe.rast[actualRast].time;
					//recipeTimeString = recipeTime+":00";
					recipeTimeString = recipeTime;
					contents.find(".time-set").html(recipeTimeString);
					contents.find(".runtime").html(currentResponseFromBM.IstTimeString+" "+bmtext[92]);
				}
				
				
				
				
				
				buildAndShowRecipe();
				showScreen(contents);
			break;
			/* Automatik break up mash.*/
			/*case(currentResponseFromBM.BrauStatus>=2810 && currentResponseFromBM.BrauStatus<2820):
			
				selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
				$(".pageTitle").html(selectedRecipe.name+" - "+bmtext[63]);
				content = pages.message.find(".display-content-row").eq(0).clone();
				content.find("h2").html(bmtext[25]);
				content.find("p").html(bmtext[30]);
				buildAndShowRecipe();
				showScreen(content);
			break;*/
			/*automatic rast ends*/
			case(currentResponseFromBM.BrauStatus>=2910 && currentResponseFromBM.BrauStatus<2930):
				selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
				$(".pageTitle").html(selectedRecipe.name+" - "+bmtext[17]);
				contentString = bmtext[11];
				contents = pages.message.find(".display-content-row").clone();
				content = contents.eq(0);
				content.find("h2").html(bmtext[17]);
				buildAndShowRecipe();
				showScreen(contents);
			break;
			/*automatic remove malt tube*/
			case(currentResponseFromBM.BrauStatus>=2930 && currentResponseFromBM.BrauStatus<2960):
				selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
				$(".pageTitle").html(selectedRecipe.name+" - "+bmtext[18]);
				contentString = bmtext[11];
				contents = pages.automatik_step.find(".display-content-row").clone();
				content = contents.eq(1);
				content.find("h2").html(bmtext[18]);
				buildAndShowRecipe();
				showScreen(content);
			break;
			/*automatic start hop cooking*/
			case(currentResponseFromBM.BrauStatus>=2960 && currentResponseFromBM.BrauStatus<3010):
				selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
				$(".pageTitle").html(selectedRecipe.name+" - "+bmtext[20]);
				contentString = bmtext[11];
				contents = pages.automatik_step.find(".display-content-row").clone();
				content = contents.eq(1);
				content.find("h2").html(bmtext[20]);
				buildAndShowRecipe();
				showScreen(content);
			break;
			/*Automatic hop cooking heat to temp.*/
			case(currentResponseFromBM.BrauStatus>=3010 && currentResponseFromBM.BrauStatus<3070):
				selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
				contents = pages.automatik_step_tt.find(".display-content-row").clone();
				
				$(".pageTitle").html(selectedRecipe.name+" - "+bmtext[8]);
				temperaturString = selectedRecipe.hopfengaben.temp.toFixed(1)+" / "+currentResponseFromBM.IstTemp.toFixed(1)+currentResponseFromBM.unit;
				
				contents.find(".fixTemp").html(selectedRecipe.hopfengaben.temp.toFixed(0));
				contents.find(".temperature-actual").html(currentResponseFromBM.IstTemp.toFixed(1)+currentResponseFromBM.unit);
				
				recipeTime = selectedRecipe.hopfengaben.duration;
				var recipeTimeArray = recipeTime.split(":");
				if(recipeTimeArray.length>1){
					recipeTimeString = recipeTime;
				}else{
					recipeTimeString = recipeTime;
				}
				contents.find(".time-set").html(recipeTimeString);
				contents.find(".runtime").html(currentResponseFromBM.IstTimeString+" "+bmtext[92]);
				
				if(currentResponseFromBM.editMode == "T"){
					console.log("edit temp");
					//Edit Temperatur
					var tempValue = contents.find(".fixTemp");
					tempValue = parseInt(tempValue);
					var tempInput = $("<input>");
					tempInput.prop("name","t").addClass("tempInput");
					tempInput.val(tempValue);
					contents.find(".fixTemp").addClass("edit").empty().append(tempInput);
					
				}else if(currentResponseFromBM.editMode == "Z"){
					console.log("Edit mode Time started");
					//Edit time time-set
					var timeValue = contents.find(".time-set");
					timeValue = parseInt(timeValue);
					var timeInput = $("<input>");
					timeInput.prop("name","t").addClass("timeInput");
					timeInput.val(timeValue);
					contents.find(".time-set").addClass("edit").empty().append(timeInput);
				}
				
				
				buildAndShowRecipe();
				showScreen(contents);
			break;
			/*Automatic hop cooking, add hop (the add hop order)*/
			case(currentResponseFromBM.BrauStatus>=3080 && currentResponseFromBM.BrauStatus<3450):
				selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
				contents = pages.automatik_step_tt_hop.find(".display-content-row").clone();
				$(".pageTitle").html(selectedRecipe.name+" - "+bmtext[5]);
				contents.eq(0).find("h2").html(bmtext[33]);
				contents.eq(0).find("p").html((currentResponseFromBM.hopfenGabe+1)+bmtext[34]+selectedRecipe.hopfengaben.gaben.length);
				buildAndShowRecipe();
				showScreen(contents);
			break;
			/*Automatic hop cooking cook hop (the add hop order)*/
			case(currentResponseFromBM.BrauStatus>=3070 && currentResponseFromBM.BrauStatus<3080):
				selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
				$(".pageTitle").html(selectedRecipe.name+" - "+bmtext[8]);
				contents = pages.automatik_step_tt.find(".display-content-row").clone();
				
				contents.find(".fixTemp").html(selectedRecipe.hopfengaben.temp.toFixed(0));
				contents.find(".temperature-actual").html(currentResponseFromBM.IstTemp.toFixed(1)+currentResponseFromBM.unit);
				
				recipeTime = selectedRecipe.hopfengaben.duration;
				var recipeTimeArray = recipeTime.split(":");
				if(recipeTimeArray.length>1){
					recipeTimeString = recipeTime;
				}else{
					recipeTimeString = recipeTime;
				}
				contents.find(".time-set").html(recipeTimeString);
				contents.find(".runtime").html(currentResponseFromBM.IstTimeString+" "+bmtext[92]);
				
				if(currentResponseFromBM.editMode == "T"){
					console.log("edit temp");
					//Edit Temperatur
					var tempValue = contents.find(".fixTemp");
					tempValue = parseInt(tempValue);
					var tempInput = $("<input>");
					tempInput.prop("name","t").addClass("tempInput");
					tempInput.val(tempValue);
					contents.find(".fixTemp").addClass("edit").empty().append(tempInput);
					
				}else if(currentResponseFromBM.editMode == "Z"){
					console.log("Edit mode Time started");
					//Edit time time-set
					var timeValue = contents.find(".time-set");
					timeValue = parseInt(timeValue);
					var timeInput = $("<input>");
					timeInput.prop("name","t").addClass("timeInput");
					timeInput.val(timeValue);
					contents.find(".time-set").addClass("edit").empty().append(timeInput);
				}
				
				buildAndShowRecipe();
				showScreen(contents);
			break;
			case(currentResponseFromBM.BrauStatus>=3450 && currentResponseFromBM.BrauStatus<=3500):
				selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
				$(".pageTitle").html(selectedRecipe.name+" - "+bmtext[22]);
				content = pages.message.find(".display-content-row").eq(0).clone();
				content.find("h2").html(bmtext[22]);
				buildAndShowRecipe();
				showScreen(content);
			break;
			case(currentResponseFromBM.BrauStatus>=3500 && currentResponseFromBM.BrauStatus<=3600):
				selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
				$(".pageTitle").html(selectedRecipe.name+" - "+bmtext[63]);
				content = pages.message.find(".display-content-row").eq(0).clone();
				content.find("h2").html(bmtext[63]);
				content.find("p").html(bmtext[64]+" "+bmtext[65]);
				buildAndShowRecipe();
				showScreen(content);
			break;
			case(currentResponseFromBM.BrauStatus>=3600 && currentResponseFromBM.BrauStatus<=3700):
				selectedRecipe = allRecipes.getRecipeByIndex(currentResponseFromBM.aktRezept);
				$(".pageTitle").html(selectedRecipe.name+" - "+bmtext[63]);
				content = pages.message.find(".display-content-row").eq(0).clone();
				content.find("h2").html(bmtext[43]);
				//content.find("p").html(bmtext[64]+" "+bmtext[65]);
				buildAndShowRecipe();
				showScreen(content);
			break;
			/*manual control*/
			case(currentResponseFromBM.BrauStatus>=4006 && currentResponseFromBM.BrauStatus<=4007):
				$(".pageTitle").html(bmtext[56]);
				content = pages.automatik_step_tt.find(".display-content-row").eq(0).clone();
				//temperatur editing mode is active. 
				if(currentResponseFromBM.editMode == "T"){
					var tempValue = content.find(".fixTemp");
					tempValue = parseInt(tempValue);
					var tempInput = $("<input>");
					tempInput.prop("name","t").addClass("tempInput");
					tempInput.val(tempValue);
					content.find(".fixTemp").addClass("edit").empty().append(tempInput);
				}
				showScreen(content);
			break;			
			/*Brewmaster shows config-screens.*/
			case(currentResponseFromBM.BrauStatus>=8000):
				$(".pageTitle").html(bmtext[49]);
				contents = pages.message.find(".display-content-row").clone();
				contents.find("h2").html(bmtext[49]);
				contents.find("p").html(bmtext[100]);
				showScreen(contents);
			break;
			/* edit recipe screen.*/
			case(currentResponseFromBM.BrauStatus>6000 && currentResponseFromBM.BrauStatus<8000):
				$(".pageTitle").html(bmtext[98]);
				contents = pages.message.find(".display-content-row").clone();
				contents.find("h2").html(bmtext[98]);
				contents.find("p").html(bmtext[99]);
				showScreen(contents);
			break;
		}
	}
	//updateStates();
	//updateScreen();
	
};
/*
 * showScreen();
 * @param content contains the jQuery- or html-object for the new display content.
 * @return nothing 
*/
var showScreen = function(content){
	renameButtons();
	$(".display-content .display-content-inner").empty().append(content);
	//buildAndShowRecipe();
	updateScreen();
	$('.display-screen').removeClass('load');
};

/* getCurrentBMStatus()
 * get the current Status from the BM.
 * @deprecated: This function will be removed after class communication is 
*/
var getCurrentBMStatus = function(dataString,updateScreens,async,returnAjax){	
	var oldBMStatus = currentResponseFromBM.BrauStatus;
	var oldBMPumpenStatus = currentResponseFromBM.pumpe;
	var oldBMHeizungStatus = currentResponseFromBM.heizung;
	currentResponseStringFromBM = "";
	
	var returnObject;
	if(dataString === "" || dataString === undefined){
		returnObject = $.ajax({
			url:bmstat,
			//cache:false,
			crossDomain:true,
			timeout:callTimeout,
			method:'GET',
			async:async,
		}).done(function(data){
			console.log("");
			currentResponseStringFromBM = data;
			intervalStopped = false;
			var lastLng = currentResponseFromBM.language;
			oldBMStatus = currentResponseFromBM.BrauStatus;
			oldBMPumpenStatus = currentResponseFromBM.pumpe;
			oldBMHeizungStatus = currentResponseFromBM.heizung;
			oldSelectedRecipe = currentResponseFromBM.aktRezept;
			oldInterrupt = currentResponseFromBM.interrupt;
			getCurrentResponseObject();
			if(lastLng!=currentResponseFromBM.language)getLanguages(currentResponseFromBM.language);
			if((updateScreens && (currentResponseFromBM.BrauStatus!==oldBMStatus))||(updateScreens && (currentResponseFromBM.BrauStatus==oldBMStatus) && (currentResponseFromBM.pumpe != oldBMPumpenStatus || currentResponseFromBM.interrupt != oldInterrupt))){
				if(currentResponseFromBM.BrauStatus!== 2300 && currentResponseFromBM.BrauStatus!==2310){
					buildScreen();
				}else{
					if(oldBMStatus<2300){
						console.log("oldBMStatus<2300 is true");
						buildScreen();
					}else{
						updateScreen();	
					}
				}
			}else{
				console.log("New Data: "+currentResponseFromBM.rawData);
				updateScreen();
				if(oldSelectedRecipe!=currentResponseFromBM.aktRezept){
					buildAndShowRecipe();
				}
			}
			stateInterval = setTimeout(updateStates, shortterm);
		}).fail(function(obj,textStat,error){
			console.log("Error date: "+ (new Date()));
			console.log("textStat = "+textStat);
			console.log("error = "+error);
			console.log("updateScreens = "+updateScreens);
			setTimeout(recallGetCurrentBMStatus, 600,"",updateScreens,async,returnAjax);
		});
	}else{
		console.log("daten übertragen.");
		console.log(dataString);
		currentResponseStringFromBM = dataString;
		getCurrentResponseObject();
		if(updateScreens){
			buildScreen();
		}
		intervalStopped = false;
		stateInterval = setTimeout(updateStates, shortterm);
	}
	if(returnAjax){
		return returnObject;
	}
};
var recallGetCurrentBMStatus = function(data,updateScreens,async,returnAjax){
	console.log("data = "+data);
	console.log("updateScreens = "+updateScreens);
	console.log("async = "+async);
	console.log("returnAjax = "+returnAjax);
	getCurrentBMStatus(data,updateScreens,async,returnAjax);
};
/* setBMStatus()
 *	@params: statusId => index of button. 
 */
var setStatusCounter = 0;
var setBmStatus = function(statusId,updateScreen,async){
	
	
	
	setBMStatusFunctionVars = [statusId,updateScreen,async];
	$.ajax({
		url:bmstat,
		data:"k="+statusId,
		//cache:false,
		crossDomain:true,
		method:'GET',
		timeout:callTimeout,
		async:async,
	}).done(function(data){
		console.log("recived data. ");
		setStatusCounter = 0;
		getCurrentBMStatus(data,updateScreen,false);
	}).fail(function(obj,textStat,error){
		console.log("Fehler Datum: "+ (new Date()));
		if(isNaN(setStatusCounter)){
			setStatusCounter = 0;
		}
		setStatusCounter++;
		if(setStatusCounter<10){
			setTimeout(function(){
				setBmStatus(setBMStatusFunctionVars[0],setBMStatusFunctionVars[1],setBMStatusFunctionVars[2]);
			},600);
		}
	});
};

/*
 * updateStates()
 * function to update the state of the bm in defined intervals. 
 * button-actions interrupt these function.
 * @return nothing
*/
var updateStates = function(){
	secondssincestart = 0;
	console.log("statusinterval aufgerufen.");
	clearTimeout(stateInterval); //delete the old interval if it is still running (only to make sure)	
	if(intervalStopped !==true){
		//get Data, write new Interval.
	//	console.log("interval aktiv. hole Daten vom Braumeister. ");
		getCurrentBMStatus("",true,true,false);		
	}
};
/*
 *	register event handlers for Buttons. 	
 */
$(document).on("click",".bmbutton",function(evtObj){
	if($(this).hasClass("disabled")===false){
		intervalStopped = true;
		var buttonNumber = parseInt($(this).index())+1;
		if((buttonNumber===2 || buttonNumber===3)&&(currentResponseFromBM.BrauStatus >=2000 && currentResponseFromBM.BrauStatus <=2020)){
			//Screen recipe-list is active, do not load recipelist again, only set active recipe to new recipe and redner recipe.
			allRecipes.reselectRecipe(buttonNumber);
		}else if((buttonNumber===2 || buttonNumber===3)&& (currentResponseFromBM.BrauStatus <1000)){
			//Startpage, check if recieps are loaded and recipeobjects are ready
			if($(this).hasClass("disabled")){
				//recipes not loaded. Ask user to load recipes 
			}else{
				$('.display-screen').addClass('load');
				cObject.callBM("k="+buttonNumber);
				
				setBmStatus(buttonNumber,true,false);
				evtObj = "";
			}		
		}else if(buttonNumber === 4 && currentResponseFromBM.BrauStatus>4006 && currentResponseFromBM.BrauStatus<=4007 && currentResponseFromBM.editMode=="T"){
			if(currentResponseFromBM.SollTemp==$("input[name='t']").val()){
				$('.display-screen').addClass('load');
				setBmStatus(buttonNumber,true,false);
				evtObj = "";
			}else{
				if(cObject.callBM("t="+parseInt($("input[name='t']").val()),bmstat,'GET')!=false){
					setTimeout(function(){
						setBmStatus(4,true,false);
					},callTimeout);
				}
			}
		}else if(currentResponseFromBM.BrauStatus===100 && buttonNumber === 4){
			window.location.reload();
		}else if(currentResponseFromBM.BrauStatus===0){
			$('.display-screen').addClass('load');
		}else{
			$('.display-screen').addClass('load');
			$(this).addClass("current");
			console.log("clicked this.");
			setBmStatus(buttonNumber,true,false);
			evtObj = "";
		}
	}

}).on("click",".dbutton",function(evtObj){
	if($(this).hasClass("disabled")===false){
		intervalStopped = true;
		var buttonNumber = parseInt($(this).index())+1;
		if((buttonNumber===2 || buttonNumber===3)&&(currentResponseFromBM.BrauStatus >=2000 && currentResponseFromBM.BrauStatus <=2020)){
			allRecipes.reselectRecipe(buttonNumber);
			//Screen recipe-list is active, do not load recipelist again, only set active recipe to new recipe and redner recipe.
		}else if((buttonNumber===2 || buttonNumber===3)&& (currentResponseFromBM.BrauStatus <1000)){
			//Startpage, check if recieps are loaded and recipeobjects are ready
			if($(this).hasClass("disabled")){
				//recipes not loaded. Ask user to load recipes 
			}else{
				$('.display-screen').addClass('load');
				setBmStatus(buttonNumber,true,false);
				evtObj = "";
			}
		}else if(buttonNumber === 4 && currentResponseFromBM.BrauStatus>4006 && currentResponseFromBM.BrauStatus<=4007 && currentResponseFromBM.editMode=="T"){
			if(currentResponseFromBM.SollTemp==$("input[name='t']").val()){
				$('.display-screen').addClass('load');
				setBmStatus(buttonNumber,true,false);
				evtObj = "";
			}else{
				if($("input[name='t']").val()>103){
					$("input[name='t']").val(103);
				}
				if(cObject.callBM("t="+parseInt($("input[name='t']").val()),bmstat,'GET')!=false){
					setTimeout(function(){
						setBmStatus(4,true,false);
					},callTimeout);
				}
			}
		}else if(currentResponseFromBM.BrauStatus===100 && buttonNumber === 4){
			window.location.reload();
			
		}else if(currentResponseFromBM.BrauStatus==0){
			$('.display-screen').addClass('load');
			//initApplication();
		}else{
			$('.display-screen').addClass('load');
			setBmStatus(buttonNumber,true,false);
			evtObj = "";
		}
	}
});
/*
 * function timer()
 * 	this function increments the actualisation timer. Also increments the Time in .time-actual and .runtime in automatic mode.
*/
var timer = function(){
        secondssincestart++;
        console.log ("timer seconds: " + secondssincestart.toString());
	$('.header-display-status-link-text').html(bmtext[78] + (secondssincestart) + bmtext[79]);
	/*use this to update actual-Time if necessarry.*/
	if((currentResponseFromBM.BrauStatus>=2810 && currentResponseFromBM.BrauStatus<2910)||(currentResponseFromBM.BrauStatus>=3070 && currentResponseFromBM.BrauStatus<3080)){
		currentResponseFromBM.IstTime = currentResponseFromBM.IstTime +1;
		var istTimeSeconds = currentResponseFromBM.IstTime % 60;
		var istTimeMinutes  = Math.floor((currentResponseFromBM.IstTime/60));
		if(currentResponseFromBM.BrauStatus!=2810){
			$("body").find(".time-actual").html((istTimeMinutes+":"+((istTimeSeconds<10)?"0"+istTimeSeconds:istTimeSeconds)));
			$("body").find(".runtime").html((istTimeMinutes+":"+((istTimeSeconds<10)?"0"+istTimeSeconds:istTimeSeconds))+" "+bmtext[92]);
		}
	}
};

/* 
 * initApplication();
 * this 	
*/
var initApplication = function(){
	allRecipes = new RecipesObject();
	cObject.getStart();
};


$(document).ready(function(){
	soundHandle = document.getElementById('soundHandle');
	soundHandle.src = 'steuerung/layout/audio/bing.wav';
    soundHandle.loop = true;
    //soundHandle.play();
	soundHandle.pause();
	$(".display-navigation").prepend($("<div>").addClass("versioninfo").html(version));
	intervalStopped = true;
	startMs = Date.now();
	var seiten = $(".page").detach();
	brewOptions = $('.brew-options');
	rastPrototype = $(".brew-process .rast").detach();
	hoppingPrototype = $(".brew-process #hops .hoppings-ul li").detach();
	seiten.each(function(){
		pages[$(this).prop("id")] = $(this);
	});
	allRecipes = new RecipesObject();
	cObject = new communication();
	currentResponseStringFromBM = "v0.0.0 Jan 01 1970;00000000000;0X0X0X0X0X0X0X0X0X0X0X0X0000XphX000"; // Datenstring für Initialen Status
	getCurrentResponseObject();
	$(".pageTitle").html(bmtext[101]);
	contents = pages.automatik_step.find(".display-content-row").eq(1).clone();
	contents.find("h2").html(bmtext[101]);
	contents.find("p").html(" ");
	renameButtons();
	$(".display-content .display-content-inner").empty().append(contents);
	$('.display-screen').removeClass('load');
	initApplication();

});
