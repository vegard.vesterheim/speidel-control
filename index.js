var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        play_sound: false,
        temp: 0,
        warn_temp: 22,
        monitoring_active: false,
        // speidel: '10.0.0.82',
        speidel: undefined,
        is_online: false,
        polling: undefined,
        polling_interval: 5000,
        comparing: 'less',
    },
    //     created: {
    //
    //     },
    methods: {
        stop_sound() {
            this.play_sound = false;
            console.log ("Stopping sound");
        },
        toggle_comparing() {
            if (this.comparing === 'greater') {
                this.comparing = 'less';
            } else {
                this.comparing = 'greater'
            }
        },
        ip_updated() {
            window.localStorage.setItem('speidel', this.speidel);
            this.poll_temp();
        },
        poll_temp() {
            this.message = "Checking temp ...";
            console.log ("Polling temp...");
            axios.get("http://"+this.speidel+"/bm.txt")
                .then(function(response) {
                    var temptxt = response.data.split(';')[2].split('X')[5];
                    app.is_online = true;
                    app.message = "Speidel is online";
                    app.temp = parseFloat(temptxt);
                    console.log ("Got temp "+temptxt);
                }).catch((error) => {
                    app.is_online = false;
                    app.message = "Error in fetching temp: "+error+". Check address!";
                    app.message_style = "alert alert-danger";
                    console.log ("error fetching temp"+error);
                });
        },
        resume_monitoring() {
            document.getElementById('soundHandle').play();
            console.log("Resume monitoring");
            this.monitoring_active = true;
            this.play_sound = false;
            this.poll_temp();

            this.polling = setInterval (this.poll_temp, this.polling_interval);
        },
        stop_monitoring() {
            console.log("Stop monitoring");
            this.monitoring_active = false;
            this.play_sound = false;
            this.temp = 0;
            clearInterval(this.polling);
            this.polling = false;
        }
    },
    beforeDestroy () {
	clearInterval(this.polling)
    },
    mounted() {
        var urlParams = new URLSearchParams(window.location.search);

        if (urlParams.get('speidel')) {
            this.speidel = urlParams.get('speidel');
            localStorage.setItem('speidel', this.speidel);
        } else if (localStorage.getItem('speidel')) {
            this.speidel = localStorage.getItem('speidel');
        } else {
            this.speidel = 'speidel'
        }
        this.poll_temp();

    },
    computed: {
        diff: function () {
            let diff = this.temp - this.warn_temp;
            if ((this.comparing === 'less' && diff <= 0) ||
                (this.comparing === 'greater' && diff >= 0)) {
                if (this.monitoring_active) {
                    console.log ("Temp is %f, %f %s than %f, activating sound",
                                 this.temp, diff, this.comparing, this.warn_temp, diff);
                    this.play_sound = true;
                }
            } else {
                console.log ("Diff is ok");
                this.play_sound = false;
            }
            return diff;
        },
    },
    watch: {
        temp: function () {
        },
        speidel: function () {
        },
        play_sound: function () {
            if (this.play_sound) {
                document.getElementById('soundHandle').play();
                document.getElementById('soundHandle').loop = true;
            } else  {
                document.getElementById('soundHandle').pause();
                document.getElementById('soundHandle').loop = true;
            }
        }
    }
});
