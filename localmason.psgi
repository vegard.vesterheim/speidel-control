#!/usr/bin/perl --
use strict; use warnings;
use Plack::Builder;
use Plack::Runner;
use Plack::App::Directory;
use Plack::App::CGIBin;
use HTML::Mason::PlackHandler;
use Plack::MIME;

# my $mime = Plack::MIME->mime_type(".wav"); # 
#  
# # register new type(s)
# Plack::MIME->add_type(".foo" => "audio/wav");
 
my $mason = HTML::Mason::PlackHandler->new(comp_root => $ENV{HOME}.'/src/speidel-control');

my $app = builder {
    enable "Plack::Middleware::Static",
        path => qr{^/data/}, root => './',
    mount "/backgrounds" => Plack::App::Directory->new({
        root => './backgrounds',
    })->to_app;
    mount "/layout" => Plack::App::Directory->new({
        root => './steuerung/layout',
    })->to_app;
    mount "/steuerung" => Plack::App::Directory->new({
        root => './steuerung',
    })->to_app;
    mount "/" => sub {
        my $env = shift;
        $mason->handle_request($env);
    };
};
my $runner = Plack::Runner->new;
$runner->parse_options( qw' --host 0.0.0.0 --port 8008 ');
$runner->run($app);

